# Projet	ProgConc Mai 2018
## Par Benjamin Currat, N�ville Dubuis et Laurent Fr�re

### Comment utiliser ce projet ?
Installer ce projet JAVA dans votre IDE. Lancer la m�thode *main* dans la classe *App* 

c'est tout!

### Que fait ce projet ?
Il charge un fichier contenant la description d'un r�seau de P�tri tr�s simple ainsi que certaines conditions pour son ex�cution.
Puis il l'ex�cute et affiche le d�roulement des actions dans une console.

### Comment est con�u ce projet ?
Ce projet a �t� divis� en trois parties majeures:

* L'aspect dynamique du test. Objet *Scheduler*.
* L'architecture d'un r�seau de P�tri simple ind�pendemment de la fonction � simuler. Objet *PetriMatrix*.
* La lecture d'un fichier JSON d�crivant le r�seau de P�tri � executer ainsi que les conditions d'initialisation et de dynamisme. Objet *PetriConfig*.

Le reste des objets sont des objets permettant de structurer le code comme par exemple la d�finition d'une matrice. 

### Que doit on encore am�liorer ?
* Ajouter des fonctions pour g�rer des arcs sp�cialis�s comme les arcs inhibiteurs.
* Ajouter des fonctions pour g�rer des crit�res de transitions plus �voluer (timer sur un arc, etc.)
* Modifier la structure multithreading pour les r�seaux avec beaucoup de transitions.

Et plus encore !