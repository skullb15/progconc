/**
 * Thread lauchned by scheduler
 */
public class Vehicle implements Runnable {
    private PlayRoom playRoom;
    private int velocity;
    private int location;

    public Vehicle(PlayRoom pArea, int pVelocityInMs) {
        playRoom = pArea;
        velocity = pVelocityInMs;
        location = 0;
    }

    public void move() throws InterruptedException {
        playRoom.getSemaphore().acquire();
        if (playRoom.canAdvance(location)) {
            System.out.println("vehicule se déplace dans " + playRoom.getName() + " de la case " + location + " à " + (location + 1));
            playRoom.setOccupiedSquare(location + 1);
            playRoom.setFreeSquare(location);
            location += 1;
        }
    }

    @Override
    public void run() {
        while (location < playRoom.getSquaresNumber() - 1) {
            try {
                Thread.currentThread().sleep(velocity);
            } catch (InterruptedException e) {
                System.out.println("Sleep error");
            }

            try {
                move();
            } catch (InterruptedException e) {
                System.out.println("Interrupt Error");
            } finally {
                playRoom.getSemaphore().release();
            }
        }

        System.out.println("Vehicule sort de la zone");
        playRoom.removeVehicle(this);
        playRoom.setFreeSquare(playRoom.getSquaresNumber() - 1);
        return;
    }
}
