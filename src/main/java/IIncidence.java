/**
 * For the petri process : post and pre incidence
 */
public interface IIncidence {

    /**
     * @param pRow start to 1
     * @param pColumn start to 1
     * @return value or -1 if the parameter are outside the the tabl size
     */
    int getValue(int pRow, int pColumn);

    /**
     *
     * @param pRow start to 1
     * @param pColumn start to 1
     * @param pValue Set token number
     * @return true if the parameters are valid
     */
    boolean setValue(int pRow, int pColumn,int pValue);

    void print();
}
