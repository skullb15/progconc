import java.util.*;

/**
 * Band master object to simulate the petri net
 *
 * @author Ben
 */
public final class Scheduler {


    /**
     * This is the gracefull shutdown
     */
    private boolean isShuttingDown = false;

    /**
     * This is the PetriNet object which is final till the end of the simulation
     */
    private final PetriMatrix matrix;

    /**
     * this is the time in millis for the tick which is final till the end of the execution
     */
    private final int tickTime;

    /**
     * the settings of the playrooms
     */
    private final PlayRoomConfig playRoomConfig;

    /**
     * We do use that a lot then just an instance should be enough
     */
    private final Timer processClock = new Timer();
    private final Timer vehicleClock = new Timer();

    /**
     * Scheduler constructor with the matrix and tick time as parameter
     *
     * @param pMatrix        the matrix representing the petri networx
     * @param pTickTime      the time of the tick in ms
     * @param playRoomConfig the configuration of the playrooms
     */
    public Scheduler(PetriMatrix pMatrix, int pTickTime, PlayRoomConfig playRoomConfig) {
        matrix = pMatrix;
        tickTime = pTickTime;
        this.playRoomConfig = playRoomConfig;
    }

    public void stop() {
        isShuttingDown = true;
    }

    /**
     * start method will enable the processing routine every tick time
     */
    public void start() {
        TimerTask process = new ProcessTask();
        TimerTask vehicleProcess = new VehicleGenerationTask();

        // processing the petri net fella
        processClock.scheduleAtFixedRate(process, tickTime, tickTime);
        vehicleClock.scheduleAtFixedRate(vehicleProcess, tickTime, tickTime);
    }

    private final class VehicleGenerationTask extends TimerTask {
        @Override
        public void run() {
            for (String key : playRoomConfig.getPlayRoomsName()) {
                playRoomConfig.getPlayRoomByName(key).addVehicle();
            }
        }
    }

    /**
     * Internal class just to show how beautifull it could be ;)
     * especially for LF
     * <p>
     * This class is supposed to execute the job at each tick it's private to
     * the Scheduler as no one else needs it's access
     * <p>
     * Walk every transition randomly between 1 and matrix.getTransitionNumber
     * Then launch evaluationTransition
     */
    private final class ProcessTask extends TimerTask {

        @Override
        public void run() {
            List<Integer> solution = new ArrayList<>();
            for (int i = 1; i <= matrix.getTransitionNumber(); i++) {
                solution.add(i);
            }
            Collections.shuffle(solution);

            for(int index : solution) {
                // activate transition on run
                matrix.activateTransition(index);
            }
        }
    }

    /**
     * ActivationTask object is just a Runnable triggered by time millis to
     * mimic the shuffle evaluation of transitions
     */
    private final class ActivationTask extends TimerTask {

        /**
         * Transition number of interest for evaluation
         */
        private final int transitionNumber;

        /**
         * ActivationTask object takes a transition as parameter
         *
         * @param pTransitionNumber the transition where to activate the task
         */
        public ActivationTask(int pTransitionNumber) {
            transitionNumber = pTransitionNumber;
        }

        @Override
        public void run() {


            if (isShuttingDown && matrix.canStop()) {
                processClock.cancel();
                processClock.purge();
            }
        }
    }
}

