import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/**
 * generic petri network
 * <p>
 * Remark:
 * There isn't safety process to avoid to use the init methods if the petri network is  running (status management)
 */
public class PetriMatrix {
    private ICurrentToken currentToken;
    private IIncidence preIncidence;
    private IIncidence postIncidence;
    private int placeNumber = 0;
    private int transitionNumber = 0;
    private boolean placeWithExternalInjection[];
    private String placeNames[];
    private String transitionsNames[];
    private String transitionDetectors[];
    private String toggleTrafficLight[];

    private Map<String, PlayRoom> playRooms = new HashMap<>();

    /**
     * @param pPlaceNumber      Initialize the place number to define the matrix size
     * @param pTransitionNumber Initialize the transition number to define the matrix size
     */
    public PetriMatrix(int pPlaceNumber, int pTransitionNumber) {
        placeNumber = pPlaceNumber;
        transitionNumber = pTransitionNumber;
        currentToken = new MatrixOneDimension(pPlaceNumber);
        preIncidence = new MatrixTwoDimension(pPlaceNumber, pTransitionNumber);
        postIncidence = new MatrixTwoDimension(pPlaceNumber, pTransitionNumber);
        placeWithExternalInjection = new boolean[pPlaceNumber];
        placeNames = new String[pPlaceNumber];
        transitionsNames = new String[pTransitionNumber];
        transitionDetectors = new String[pTransitionNumber];
        toggleTrafficLight = new String[pPlaceNumber];
    }

    /**
     * @param pPlace place using an external input token
     * @return true if the method has been executed( avoid to raise an exception)
     */
    public boolean defineThePlaceWithAnExternalInputToken(int pPlace) {
        if (pPlace > placeNumber) {
            return false;
        }
        placeWithExternalInjection[pPlace - 1] = true;
        return true;
    }

    /**
     * @param pPlace place using an external input token
     * @return true if the method has been executed( avoid to raise an exception)
     */
    public boolean defineThePlaceWithTokenDesctuction(int pPlace) {
        if (pPlace > placeNumber) {
            return false;
        }
        placeWithExternalInjection[pPlace - 1] = true;
        return true;
    }

    /**
     * @param pPlace the place to set the name
     * @param name   the name of the place
     * @return true if the method has been executed( avoid to raise an exception)
     */
    public boolean definePlaceName(int pPlace, String name) {
        if (pPlace > placeNumber) {
            return false;
        }
        placeNames[pPlace - 1] = name;
        return true;
    }

    /**
     * @param pPlace the place to get the name
     * @return the name of the place (or empty string if not in place range)
     */
    public String getPlaceName(int pPlace) {
        if (pPlace > placeNumber) {
            return "";
        }
        return placeNames[pPlace - 1];
    }

    /**
     * @param pTransition the transition to set the name
     * @param name        the name of the transition
     * @return true if the method has been executed( avoid to raise an exception)
     */
    public boolean defineTransitionName(int pTransition, String name) {
        if (pTransition > transitionNumber) {
            return false;
        }
        transitionsNames[pTransition - 1] = name;
        return true;
    }

    public boolean definePlayRoom(String pLane, PlayRoom pPlayRoom) {
        playRooms.put(pLane, pPlayRoom);
        return true;
    }

    public boolean defineTansitionDetector(int pTransition, String detector) {
        if (pTransition > transitionNumber) {
            return false;
        }
        transitionDetectors[pTransition - 1] = detector;
        return true;
    }

    public boolean defineToggleTrafficLight(int pPlace, String trafficLight) {
        if (pPlace > placeNumber) {
            return false;
        }
        toggleTrafficLight[pPlace - 1] = trafficLight;
        return true;
    }

    /**
     * @param pTransition the transition to get the name
     * @return the name of the place (or empty string if not in place range)
     */
    public String getTransitionName(int pTransition) {
        if (pTransition > transitionNumber) {
            return "";
        }
        return transitionsNames[pTransition - 1];
    }


    /**
     * TODO careful with that as we had to change the generator and it's not into the same thread as activation anymore
     *
     * @return
     */
    public boolean addExternalInputToken() {

        boolean isSuccess = true;
        final Random random = new Random();

        boolean externalTokenNotFund = true;
        int iterationNumber = 0;
        do {
            iterationNumber++;
            int randomPlace = random.nextInt(placeNumber) + 1;
            if (placeWithExternalInjection[randomPlace - 1]) {
                externalTokenNotFund = false;
                currentToken.addValue(randomPlace, 1);

            } else if (iterationNumber > (2 * placeNumber)) {
                isSuccess = false;

            }

        } while (externalTokenNotFund && isSuccess);
        return isSuccess;
    }

    /**
     * @param pPlace specify the place to ask
     * @return the token number present in this place
     */
    public int getTokenNumber(int pPlace) {
        return currentToken.getValue(pPlace);
    }

    /**
     * initialize the petri network with some token before to run this one
     *
     * @param pPlace       Place concerned
     * @param pTokenNumber Token number required before to run the petri network
     * @return true for valid initialization, false if not
     */
    public boolean initializeTokenNumber(int pPlace, int pTokenNumber) {
        return currentToken.setValue(pPlace, pTokenNumber);
    }


    /**
     * @param pPlace      Place concerned
     * @param pTransition transition concerned
     * @return the token number required for a place to validate the pre incident arc of the transition
     */
    public int getTokenPreIncidence(int pPlace, int pTransition) {
        return preIncidence.getValue(pPlace, pTransition);
    }

    /**
     * Method to use only to initialize the pre incident matrix
     *
     * @param pPlace       Place concerned
     * @param pTransition  transition concerned
     * @param pTokenNumber Token required
     * @return true if the parameters are conform (avoid to raise an exception)
     */
    public boolean setTokenPreIncidence(int pPlace, int pTransition, int pTokenNumber) {
        return preIncidence.setValue(pPlace, pTransition, pTokenNumber);
    }

    /**
     * @param pPlace      Place concerned
     * @param pTransition transition concerned
     * @return the token number to add to a place if the transition considered is ok.
     */
    public int getTokenPostIncidence(int pPlace, int pTransition) {
        return postIncidence.getValue(pPlace, pTransition);
    }

    /**
     * Method to use only to initialize the post incident matrix
     *
     * @param pPlace       Place concerned
     * @param pTransition  transition concerned
     * @param pTokenNumber Token required
     * @return true if the parameters are conform (avoid to raise an exception)
     */
    public boolean setTokenPostIncidence(int pPlace, int pTransition, int pTokenNumber) {
        return postIncidence.setValue(pPlace, pTransition, pTokenNumber);
    }

    /**
     * Print the full state of the petri matrix
     */
    public void printState() {
        printState(false);
    }

    /**
     * print the state petri matrix
     *
     * @param onlyCurrent true to print only the current state of the tokens
     */
    public void printState(Boolean onlyCurrent) {
        System.out.println("Current state of tokens:");
        currentToken.print();

        if (!onlyCurrent) {
            System.out.println("Current preIncidence:");
            preIncidence.print();
            System.out.println("Current postIncidence:");
            postIncidence.print();
        }
    }

    /**
     * Method to modify the token if an action is raised on a transition
     *
     * @param pTransition transition call by the event (action)
     * @return true if the transition has been modified
     */
    public boolean activateTransition(int pTransition) {
        return activateTransitionSynchronized(pTransition);
    }

    /**
     * This approach is the best way if the petri network is small.
     * We synchronize all thread when we evaluate a transition.
     * Therefore there is a bord effect on the speed for the big network.
     * We can improve this class to handle this case (mainly for computer multicore)
     *
     * @param pTransition transition to activate
     * @return boolean value representing the success of the activation
     */
    private synchronized boolean activateTransitionSynchronized(int pTransition) {
        boolean isSuccess = false;

        if (evaluateTransition(pTransition)) {
            int i = 1;
            do {
                currentToken.addValue(i, -getTokenPreIncidence(i, pTransition));

                int token = getTokenPostIncidence(i, pTransition);
                currentToken.addValue(i, token);

                if (token > 0) {
                    String[] trafficLightAction = toggleTrafficLight[i - 1].split("#");
                    PlayRoom d = playRooms.get(trafficLightAction[0]);

                    System.out.println("Light " + trafficLightAction[0]  + " to " + trafficLightAction[1]);

                    if (trafficLightAction[1].toUpperCase().equals("GREEN")) {
                        d.getTrafficLight().toGreen();
                    } else {
                        d.getTrafficLight().toRed();
                    }
                }

                i++;
            } while (i <= placeNumber);

            isSuccess = true;
        }

        return isSuccess;
    }

    /**
     * Check if the criteria to activate the transition are ok
     *
     * @param pTransition the transition to evaluate
     * @return boolean value representing the state of te transition
     * TODO Need to add that detector thing based on area
     */
    private boolean evaluateTransition(int pTransition) {
        boolean isConform = true;
        int i = 1;
        String[] transAndAction = transitionDetectors[pTransition - 1].split("#");
        PlayRoom d = playRooms.get(transAndAction[0]);

        boolean inOrOut = "IN".equals(transAndAction[1].toUpperCase());
        Detector detector = inOrOut ? d.getDetectorIn() : d.getDetectorOut();

        do {
            if (getTokenNumber(i) < getTokenPreIncidence(i, pTransition)) {
                isConform = false;
            } else {
                isConform = inOrOut ? detector.hasVehicle() : detector.isEmpty();
            }
            i++;
        } while (i <= placeNumber && isConform);

        return isConform;
    }

    /**
     * @return the transition number defined in this petri network
     */
    public int getTransitionNumber() {
        return transitionNumber;
    }


    /**
     * TODO implement the ability to figure whether all cars have been through the petri net
     *
     * @return
     */
    public boolean canStop() {
        return false;
    }
}
