import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

class App {
    private static JSONObject jsonConfig;

    public static void main(String args[]) {
        InputStream configFile = App.class.getResourceAsStream("/carrefour.json");
        jsonConfig = parseJsonStream(configFile);

        // loading from petri matrix
        PetriConfig petriConfig = new PetriConfig(jsonConfig);

        // loading of carrefour config
        PlayRoomConfig playRoomConfig = new PlayRoomConfig(jsonConfig, petriConfig);

        // create Scheduler with basic PetriMatrix
        Scheduler s = new Scheduler(petriConfig.getPetriMatrix(), petriConfig.getTic(), playRoomConfig);

        // start the Scheduler
        s.start();
    }

    /**
     * Parss JSON file
     *
     * @param file the json stream to pars
     * @return JSONObject from the loaded file or an empty json if error in read
     */
    private static JSONObject parseJsonStream(InputStream file) {
        StringBuilder responseStrBuilder = new StringBuilder();
        BufferedReader br = new BufferedReader(new InputStreamReader(file));
        String line;

        try {
            while (((line = br.readLine()) != null)) {
                responseStrBuilder.append(line);
            }

            return new JSONObject(responseStrBuilder.toString());

        } catch (IOException e) {
            e.printStackTrace();
            return new JSONObject("{}");
        }
    }
}