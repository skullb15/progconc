import org.json.JSONObject;

import java.util.HashMap;
import java.util.Set;

public class PlayRoomConfig {
    private JSONObject jsonConfig;
    private PetriConfig petriConfig;
    private HashMap<String, PlayRoom> playRooms = new HashMap<>();

    public PlayRoomConfig(JSONObject jsonConfig, PetriConfig petriConfig) {
        this.jsonConfig = jsonConfig;
        this.petriConfig = petriConfig;

        initPlayRooms();
    }

    private void initPlayRooms() {
        JSONObject playRoomsJson = jsonConfig.getJSONObject("playRoomConfig");
        PlayRoom playRoom;

        for (Object keyObject : playRoomsJson.keySet()) {
            //based on you key types
            String key = (String)keyObject;
            JSONObject value = playRoomsJson.getJSONObject(key);

            playRoom = new PlayRoom(
                    value.getInt("squareNumber"),
                    value.getInt("trafficLightAt"),
                    value.getInt("detectorOffset"),
                    key
            );

            playRooms.put(key, playRoom);
            petriConfig.getPetriMatrix().definePlayRoom(key, playRoom);
        }
    }

    public HashMap<String, PlayRoom> getPlayRooms() {
        return playRooms;
    }

    public PlayRoom getPlayRoomByName(String name) {
        return playRooms.get(name);
    }

    public String[] getPlayRoomsName() {
        Set<String> keys = playRooms.keySet();

        return keys.toArray(new String[keys.size()]);
    }
}
