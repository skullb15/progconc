/**
 * Detector is as its name says a detector :)
 */
public class Detector {
    private final int position;
    private final int offset;
    private final PlayRoom playRoom;

    public Detector(int position, int offset, PlayRoom playRoom) {
        this.position = position;
        this.offset = offset;
        this.playRoom = playRoom;
    }

    public boolean isEmpty() {
        return isEmptySynchronized();
    }

    /**
     * TODO isEmpty
     *
     * @return
     */
    private synchronized boolean isEmptySynchronized() {
        boolean empty = true;

        for (int i = position; i < position + offset; i++ ) {
            empty = empty && playRoom.isSquareFree(i);
        }

        return empty;
    }

    /**
     * TODO hasVehicle
     *
     * @return
     */
    public boolean hasVehicle() {
        return !isEmpty();
    }

}
