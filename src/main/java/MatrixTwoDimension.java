import java.util.Arrays;

public class MatrixTwoDimension implements IIncidence{
    private int genericMatrix [] []=null;

    public MatrixTwoDimension(int pRowNumber, int pColumnNumber){
        genericMatrix = new int[pRowNumber][pColumnNumber];
    }

    @Override
    public int getValue(int pRow, int pColumn) {
        if(pRow > genericMatrix.length&& pRow<1){
            return -1;
        }
        else if (pColumn > genericMatrix[0].length&& pColumn<1) {
            return -1;
        }

        return genericMatrix[pRow-1] [pColumn-1];
    }

    @Override
    public boolean setValue(int pRow, int pColumn,int pValue){
        if(pRow > genericMatrix.length&& pRow<1){
            return false;
        }
        else if (pColumn > genericMatrix[0].length&& pColumn<1) {
            return false;
        }
        else if(pValue<0){
            return false;
        }

        genericMatrix[pRow-1] [pColumn-1]=pValue;
        return true;
    }


    public boolean addValue(int pRow, int pColumn,int pOffset) {
        if (pRow > genericMatrix.length && pRow < 1) {
            return false;
        } else if (pColumn > genericMatrix[0].length && pColumn < 1) {
            return false;
        } else if (genericMatrix[pRow - 1][pColumn - 1] + pOffset < 0) {
            return false;
        }

        genericMatrix[pRow - 1][pColumn - 1] = genericMatrix[pRow - 1][pColumn - 1] + pOffset;
        return true;
    }

    public void print() {
        for (int[] row : genericMatrix)
            System.out.println(Arrays.toString(row));
    }
}
