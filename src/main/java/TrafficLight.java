public class TrafficLight {

    private String lightColor;

    public TrafficLight() {
        lightColor = "red";
    }

    public void toGreen() {
        lightColor = "green";
        System.out.println("Light color to " + lightColor);
    }

    public void toRed() {
        lightColor = "red";
        System.out.println("Light color to " + lightColor);
    }

    public boolean isRed() {
        return lightColor == "red";
    }

    public String getLightColor() {
        return lightColor;
    }

    public void setLightColor(String lightColor) {
        this.lightColor = lightColor;
    }
}
