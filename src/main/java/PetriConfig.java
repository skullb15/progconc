import org.json.JSONArray;
import org.json.JSONObject;

public class PetriConfig {
    private PetriMatrix matrix;
    private JSONObject jsonConfig;

    public PetriConfig(JSONObject jsonConfig) {
        this.jsonConfig = jsonConfig;

        initPetriMatrix();
        matrix.printState();
    }

    /**
     * the loaded PetriMatrix
     *
     * @return the petri matrix loaded from the xml
     */
    public PetriMatrix getPetriMatrix() {
        return matrix;
    }

    /**
     * the tic value as defined in the config
     *
     * @return int representing the length of a tic in ms
     */
    public int getTic() {
        return jsonConfig.getInt("tic");
    }

    /**
     * Get the places from the json config
     *
     * @return JSONArray the content of the json file
     */
    private JSONArray getPlacesFromConfig() {
        return jsonConfig.getJSONObject("petriConfig").getJSONArray("places");
    }

    /**
     * find the index of a place based on it's id
     *
     * @param id the id to search
     * @return value or -1 if the id is not found
     */
    private int getPlaceIndexFromId(String id) {
        int index = 1;
        int found = -1;

        for (Object object : getPlacesFromConfig()) {
            JSONObject place = ((JSONObject) object);

            if (place.has("id") && place.getString("id").equals(id)) {
                found = index;
            }

            index++;
        }

        return found;
    }

    /**
     * Get the transitions from the json config
     *
     * @return JSONArray the content of the json file
     */
    private JSONArray getTransitionsFromConfig() {
        return jsonConfig.getJSONObject("petriConfig").getJSONArray("transitions");
    }

    /**
     * Get a pseudo transition object
     *
     * @param id
     * @return
     */
    private JSONObject getTansitionFromId(String id) {
        return getTransitionsFromConfig().getJSONObject(getTransitionIndexFromId(id));
    }

    private JSONObject getTansitionFromIndex(int index) {
        return getTransitionsFromConfig().getJSONObject(index);
    }

    /**
     * find the index of a transitions based on it's id
     *
     * @param id the id to search
     * @return value or -1 if the id is not found
     */
    private int getTransitionIndexFromId(String id) {
        int index = 1;
        int found = -1;

        for (Object object : getTransitionsFromConfig()) {
            JSONObject transition = ((JSONObject) object);

            if (transition.has("id") && transition.getString("id").equals(id)) {
                found = index;
            }
            index++;
        }

        return found;
    }

    /**
     * Get the pre arcs from the json config
     *
     * @return JSONArray the content of the json file
     */
    private JSONArray getPreArcsFromConfig() {
        return jsonConfig.getJSONObject("petriConfig").getJSONArray("arcsPre");
    }

    /**
     * Get the post arcs from the json config
     *
     * @return JSONArray the content of the json file
     */
    private JSONArray getPostArcsFromConfig() {
        return jsonConfig.getJSONObject("petriConfig").getJSONArray("arcsPost");
    }

    /**
     *
     * Init the PetriMatrix
     */
    private void initPetriMatrix() {
        matrix = new PetriMatrix(getPlacesFromConfig().length(), getTransitionsFromConfig().length());

        initMatrixPlaces();
        initMatrixTransitions();
        initMatrixPreIncidence();
        initMatrixPostIncidence();
    }

    /**
     * Init the current token values in the PetriMatrix
     */
    private void initMatrixPlaces() {
        int index = 1;

        for (Object object : getPlacesFromConfig()) {
            JSONObject place = ((JSONObject) object);

            if (place.has("initToken")) {
                matrix.initializeTokenNumber(index, place.getInt("initToken"));
            }

            if (place.has("name")) {
                matrix.definePlaceName(index, place.getString("name"));
            }

            if (place.has("tokenEntry") && place.getBoolean("tokenEntry")) {
                matrix.defineThePlaceWithAnExternalInputToken(index);
            }

            if (place.has("tokenExit") && place.getBoolean("tokenExit")) {
                matrix.defineThePlaceWithTokenDesctuction(index);
            }

            if (place.has("toggleTrafficLight")) {
                matrix.defineToggleTrafficLight(index, place.getString("toggleTrafficLight"));
            }

            index++;
        }
    }

    /**
     * Init all about transitions in PetriMatrix
     */
    private void initMatrixTransitions() {
        int index = 1;

        for (Object object : getTransitionsFromConfig()) {
            JSONObject transition = ((JSONObject) object);

            if (transition.has("name")) {
                matrix.defineTransitionName(index, transition.getString("name"));
                matrix.defineTansitionDetector(index, transition.getString("captor"));
            }
            index++;
        }
    }

    /**
     * Init the PetriMatrix PostIncidence
     */
    private void initMatrixPreIncidence() {
        for (Object object : getPreArcsFromConfig()) {
            JSONObject preArc = ((JSONObject) object);
            int placeIndex = getPlaceIndexFromId(preArc.getString("idPlace"));
            int transitionIndex = getTransitionIndexFromId(preArc.getString("idTransition"));
            int capacity = preArc.getInt("capacity");

            matrix.setTokenPreIncidence(placeIndex, transitionIndex, capacity);
        }
    }

    /**
     * Init the PetriMatrix preIncidence
     */
    private void initMatrixPostIncidence() {
        for (Object object : getPostArcsFromConfig()) {
            JSONObject postArc = ((JSONObject) object);
            int placeIndex = getPlaceIndexFromId(postArc.getString("idPlace"));
            int transitionIndex = getTransitionIndexFromId(postArc.getString("idTransition"));
            int capacity = postArc.getInt("capacity");

            matrix.setTokenPostIncidence(placeIndex, transitionIndex, capacity);
        }
    }
}
