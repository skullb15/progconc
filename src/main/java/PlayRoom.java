import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.Semaphore;

/**
 * This is the area where magic happens
 * Basically magic will happen with a Semaphore
 *
 *
 */
public class PlayRoom {
    private final Semaphore semaphore;
    private final TrafficLight trafficLight;
    private final int trafficLightPosition;
    private final Detector detectorIn;
    private final Detector detectorOut;
    private final int squareNumber;
    private boolean[] squares;
    private String name;
    private ArrayList<Vehicle> vehicles = new ArrayList<Vehicle>();


    public PlayRoom(int squareNumber, int pTrafficLightPos, int pNumberOfZones, String name){
        semaphore = new Semaphore(1);
        trafficLight = new TrafficLight();
        trafficLightPosition = pTrafficLightPos;
        detectorIn = new Detector((pTrafficLightPos - pNumberOfZones), pNumberOfZones, this);
        detectorOut = new Detector(pTrafficLightPos, pNumberOfZones, this);
        this.squareNumber = squareNumber;
        this.name = name;
        squares = new boolean[squareNumber];
    }

    public Detector getDetectorIn() {
        return detectorIn;
    }

    public Detector getDetectorOut() {
        return detectorOut;
    }

    public TrafficLight getTrafficLight() { return trafficLight; }

    public Semaphore getSemaphore() { return semaphore; }

    public String getName() { return name; }

    public int getSquaresNumber() {
        return squares.length;
    }

    public void setOccupiedSquare(int index) {
        squares[index] = true;
    }

    public void setFreeSquare(int index) {
        squares[index] = false;
    }

    public boolean canAdvance(int location) {
        if (isSquareFree(location + 1)) {
            if (location == trafficLightPosition - 1 && trafficLight.isRed()) {
                return false;
            } else {
                return true;
            }
        } else  {
            return false;
        }
    }

    public boolean isSquareFree(int index) {
        return !squares[index];
    }

    public Vehicle addVehicle () {
        if (isSquareFree(0)) {
            System.out.println("Ajout d'un vehicule dans la zone " + name);

            Random rand = new Random();
            int velocity = rand.nextInt(1000) + 501;
            Vehicle vehicle = new Vehicle(this, velocity);
            vehicles.add(vehicle);

            setOccupiedSquare(0);
            new Thread(vehicle).start();

            return vehicle;
        } else {
            return null;
        }
    }

    public void removeVehicle(Vehicle vehicle) {
        vehicles.remove(vehicle);
    }
}
