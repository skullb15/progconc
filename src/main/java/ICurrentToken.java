public interface ICurrentToken {

    int getValue(int pRow);

    boolean setValue(int pRow, int pValue);

    boolean addValue(int pRow, int pOffset);

    void print();
}
