public class MatrixOneDimension implements ICurrentToken {
    private MatrixTwoDimension genericMatrix;

    public MatrixOneDimension(int pRowNumber){
        genericMatrix = new MatrixTwoDimension(pRowNumber,1);
    }

     @Override
     public int getValue(int pRow){
        return genericMatrix.getValue(pRow,1);
    }

    @Override
    public boolean setValue(int pRow, int pValue){
         return  genericMatrix.setValue(pRow,1,pValue);
    }

    @Override
    public boolean addValue(int pRow,int pOffset) {return genericMatrix.addValue(pRow,1,pOffset); }

    public void print() {
        genericMatrix.print();
    }
}
